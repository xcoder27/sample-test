class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :url_name
      t.text :h1
      t.text :h2
      t.text :h3
      t.text :href

      t.timestamps null: false
    end
  end
end
