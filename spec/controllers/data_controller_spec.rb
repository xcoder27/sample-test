require 'rails_helper'
require 'spec_helper'

describe DataController do 
	describe "routing" do
		it "should routes to #fetch" do
			expect(get: '/data/fetch?url=http://testing.com').to route_to(
				controller: 'data',
				action: 'fetch',
				url: "http://testing.com"
				)
			expect(response).to have_http_status(:ok)
		end
	end
end