class Url < ActiveRecord::Base
	require 'mechanize'

	def self.Crawle_content(user_site)
		mechanize = Mechanize.new
		page = mechanize.get(user_site).body      #extracts the required site html page body
		html_doc =  Nokogiri::HTML(page)
		h1 = html_doc.css('h1')                    #extracts the H1 tage in 
		h2 = html_doc.css('h2')										 #Extracts the H2 tage 
		h3 = html_doc.css('h3')											#extract heading h3
 		href = html_doc.xpath("//a/@href")
 		Url.create(url_name: user_site, h1: h1.text, h2: h2.text , h3: h3.text , href: href.text)

 	end
end
