class DataController < ApplicationController

  def fetch
  	if Url.exists?(url_name: params[:url])
  		
  		result  = Url.where(url_name: params[:url]).select(:url_name, :h1, :h2 ,:h3 ,:href).to_json
  		return render json: result
  	else
  		begin
				insert_record = Url.Crawle_content(params[:url])
				result  = Url.where(id: insert_record.id).select(:url_name, :h1, :h2 ,:h3 ,:href).to_json
  			return render json: result
  		rescue   => e
  			render json: e.to_s
  		end
  	end
  end
end
